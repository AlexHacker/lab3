import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import com.independentsoft.office.ExtendedBoolean;
import com.independentsoft.office.IContentElement;
import com.independentsoft.office.Unit;
import com.independentsoft.office.UnitType;
import com.independentsoft.office.drawing.Extents;
import com.independentsoft.office.drawing.Offset;
import com.independentsoft.office.drawing.Picture;
import com.independentsoft.office.drawing.PresetGeometry;
import com.independentsoft.office.drawing.ShapeType;
import com.independentsoft.office.drawing.Transform2D;
import com.independentsoft.office.word.Background;
import com.independentsoft.office.word.BottomBorder;
import com.independentsoft.office.word.Color;
import com.independentsoft.office.word.Comment;
import com.independentsoft.office.word.CommentRangeEnd;
import com.independentsoft.office.word.CommentRangeStart;
import com.independentsoft.office.word.CommentReference;
import com.independentsoft.office.word.DrawingObject;
import com.independentsoft.office.word.HorizontalAlignmentType;
import com.independentsoft.office.word.HorizontalInsideBorder;
import com.independentsoft.office.word.Hyperlink;
import com.independentsoft.office.word.IBlockElement;
import com.independentsoft.office.word.LeftBorder;
import com.independentsoft.office.word.Paragraph;
import com.independentsoft.office.word.RightBorder;
import com.independentsoft.office.word.Run;
import com.independentsoft.office.word.RunContentColor;
import com.independentsoft.office.word.StandardBorderStyle;
import com.independentsoft.office.word.ThemeColor;
import com.independentsoft.office.word.TopBorder;
import com.independentsoft.office.word.Underline;
import com.independentsoft.office.word.UnderlinePattern;
import com.independentsoft.office.word.VerticalAlignment;
import com.independentsoft.office.word.VerticalInsideBorder;
import com.independentsoft.office.word.WordDocument;
import com.independentsoft.office.word.drawing.DrawingObjectSize;
import com.independentsoft.office.word.drawing.Inline;
import com.independentsoft.office.word.footnoteEndnote.Footnote;
import com.independentsoft.office.word.footnoteEndnote.FootnoteReference;
import com.independentsoft.office.word.headerFooter.Footer;
import com.independentsoft.office.word.headerFooter.Header;
import com.independentsoft.office.word.tables.Cell;
import com.independentsoft.office.word.tables.Row;
import com.independentsoft.office.word.tables.Table;
import com.independentsoft.office.word.tables.TableWidthUnit;
import com.independentsoft.office.word.tables.Width;

public class AddTable {

	public void createTable(int i, int j, WordDocument doc) {
		Run run1 = new Run();
		run1.addText("Below is one table with " + i + " rows and " + j
				+ "columns.");
		Paragraph paragraph1 = new Paragraph();
		paragraph1.add(run1);
		Run run2 = new Run();
		run2.addText("table");
		Cell cell1 = new Cell();
		Paragraph paragraph2 = new Paragraph();
		paragraph2.add(run2);
		cell1.add(paragraph2);
		Row row1 = new Row();
		for (int k = 0; k < i; k++)
			row1.add(cell1);
		Table table1 = new Table(StandardBorderStyle.SINGLE_LINE);
		table1.setWidth(new Width(TableWidthUnit.PERCENT, 100));
		for (int k = 0; k < j; k++)
			table1.add(row1);
		doc.getBody().add(paragraph1);
		doc.getBody().add(table1);
	}

	public void createFootnote(int i, String s1, String s2,
			WordDocument doc) {
		Footnote footnote1 = new Footnote(i);
		Paragraph p = new Paragraph();
		Run footnoteRun1 = new Run();
		footnoteRun1.setVerticalAlignment(VerticalAlignment.SUPERSCRIPT);
		footnoteRun1.addFootnoteReferenceMark();
		Run footnoteRun2 = new Run();
		footnoteRun2.addText(s1);
		p.add(footnoteRun1);
		p.add(footnoteRun2);
		footnote1.getContent().add(p);
		Run run1 = new Run();
		run1.addText(s2);
		Paragraph paragraph1 = new Paragraph();
		paragraph1.add(run1);
		FootnoteReference footnoteReference = new FootnoteReference(i);
		Run footnoteReferenceRun = new Run();
		footnoteReferenceRun
				.setVerticalAlignment(VerticalAlignment.SUPERSCRIPT);
		footnoteReferenceRun.add(footnoteReference);
		paragraph1.add(footnoteReferenceRun);
		doc.getFootnotes().add(footnote1);
		doc.getBody().add(paragraph1);
	}

	public void createWord(String s) throws IOException, XMLStreamException{
		
			WordDocument doc = new WordDocument();
			Picture picture = new Picture("D:\\Projects2\\wordTrain\\clean.png");
			Unit pictureWidth = new Unit(16, UnitType.PIXEL);
			Unit pictureHeight = new Unit(16, UnitType.PIXEL);

			Offset offset = new Offset(40, 20);
			Extents extents = new Extents(pictureWidth, pictureHeight);

			picture.getShapeProperties().setPresetGeometry(
					new PresetGeometry(ShapeType.RECTANGLE));
			picture.getShapeProperties().setTransform2D(
					new Transform2D(offset, extents));
			picture.setID("1");
			picture.setName("clean.png");

			Inline inline = new Inline(picture);
			inline.setSize(new DrawingObjectSize(pictureWidth, pictureHeight));
			inline.setID("1");
			inline.setName("Picture 1");
			inline.setDescription("clean.png");

			DrawingObject drawingObject = new DrawingObject(inline);
			Run run15 = new Run();
			run15.addText("�������� ������ � ������� ���������� JWord");
			run15.setFontSize(22);
			run15.setBold(ExtendedBoolean.TRUE);
			run15.setPosition(100);
			Paragraph p15 = new Paragraph();
			p15.add(run15);
			doc.getBody().add(p15);

			Run run5 = new Run();
			run5.addText("Below is an image:");

			Run run6 = new Run();
			run6.add(drawingObject);

			Paragraph paragraph6 = new Paragraph();
			Paragraph paragraph7 = new Paragraph();
			paragraph6.add(run5);
			paragraph7.add(run6);
			doc.getBody().add(paragraph6);
			doc.getBody().add(paragraph7);
			createFootnote(1, "������ Word", "Hello Word", doc);
			createFootnote(2, "������ ���", "Hello World", doc);
			Run run1 = new Run();
			run1.addText("Hello Java!");

			Paragraph paragraph1 = new Paragraph();
			paragraph1.add(run1);
			Run headerRun = new Run();
			headerRun.addText("Header text");
			Paragraph headerParagraph = new Paragraph();
			headerParagraph.add(headerRun);
			headerParagraph
					.setHorizontalTextAlignment(HorizontalAlignmentType.RIGHT);
			Header header = new Header();
			header.add(headerParagraph);
			Run footerRun = new Run();
			footerRun.addText("Footer text");
			Paragraph footerParagraph = new Paragraph();
			footerParagraph.add(footerRun);
			footerParagraph
					.setHorizontalTextAlignment(HorizontalAlignmentType.CENTER);
			Footer footer = new Footer();
			footer.add(footerParagraph);
			Run run = new Run();
			run.addText("������");
			run.setUnderline(new Underline(UnderlinePattern.SINGLE));
			Color runColor = new Color("#0000FF");
			run.setColor(new RunContentColor(runColor));
			Hyperlink link = new Hyperlink();
			link.add(run);
			link.setTarget("http://www.yandex.ru/");
			Paragraph paragraph = new Paragraph();
			paragraph.add(link);
			doc.getBody().add(paragraph1);
			doc.getBody().setHeader(header);
			doc.getBody().setFooter(footer);
			doc.getBody().add(paragraph);
			createTable(6, 4, doc);
			WordDocument second = new WordDocument(
					"D:\\Projects2\\wordTrain\\test8.docx");
			for (IBlockElement element : second.getBody().getContent()) {
				doc.getBody().add(element);
			}
			Run commentRun = new Run();
			commentRun.addText("Comment text.");
			Paragraph commentParagraph = new Paragraph();
			commentParagraph.add(commentRun);
			Comment comment1 = new Comment(1);
			comment1.setAuthor("��������� ��������");
			comment1.setDate(new Date());
			comment1.setInitials("�.�.");
			comment1.add(commentParagraph);
			CommentRangeStart commentStart = new CommentRangeStart(1);
			CommentRangeEnd commentEnd = new CommentRangeEnd(1);
			CommentReference commentReference = new CommentReference(1);
			Run commentReferenceRun = new Run();
			commentReferenceRun.add(commentReference);

			Run run2 = new Run();
			run2.addText("Hello ");

			Run run3 = new Run();
			run3.addText("Word");

			Run run4 = new Run();
			run4.addText(".");
			Paragraph paragraph24 = new Paragraph();
			paragraph24.add(run2);
			paragraph24.add(commentStart);
			paragraph24.add(run3);
			paragraph24.add(commentEnd);
			paragraph24.add(commentReferenceRun);
			paragraph24.add(run4);
			doc.getBody().add(paragraph24);
			doc.getComments().add(comment1);
			WordDocument doc1 = new WordDocument(
					"D:\\Projects2\\wordTrain\\test12.docx");
			doc1.replace("After", "before");
			String text = doc1.toText();
			Run run11 = new Run();
			run11.addText(text);
			Paragraph p11 = new Paragraph();
			p11.add(run11);
			Run run12 = new Run();
			run12.addText("���������� ������ �� ������� ���������\n___________________________________");
			Paragraph p12 = new Paragraph();
			p12.add(run12);
			doc.getBody().add(p12);
			doc.getBody().add(p11);


			List<IContentElement> elements = doc1.getContentElements();

			for (IContentElement element : elements) {
				if (element instanceof Hyperlink) {
					Hyperlink link1 = (Hyperlink) element;

					System.out.println("Target = " + link1.getTarget());
				}
			}
			Run run13 = new Run();
			run13.addText("���������� ������� �� ������� ���������\n___________________________________");
			Paragraph p13 = new Paragraph();
			p13.add(run13);
			 doc.getBody().add(p13);

			List<Table> tables = doc1.getTables();

			for (Table table : tables) {
				table.setTopBorder(new TopBorder(
						StandardBorderStyle.DOUBLE_LINE));
				table.setBottomBorder(new BottomBorder(
						StandardBorderStyle.DOUBLE_LINE));
				table.setLeftBorder(new LeftBorder(
						StandardBorderStyle.DOUBLE_LINE));
				table.setRightBorder(new RightBorder(
						StandardBorderStyle.DOUBLE_LINE));
				table.setHorizontalInsideBorder(new HorizontalInsideBorder(
						StandardBorderStyle.DOUBLE_LINE));
				table.setVerticalInsideBorder(new VerticalInsideBorder(
						StandardBorderStyle.DOUBLE_LINE));
				doc.getBody().add(table);
			}
			Run run10 = new Run();
            run10.addText("See background color!");

            Paragraph paragraph10 = new Paragraph();
            paragraph10.add(run10);

            doc.getBody().add(paragraph10);

            Background background = new Background();
            background.setThemeColor(ThemeColor.ACCENT_5);

            doc.setBackground(background);
            doc.getSettings().setDisplayBackgroundShape(ExtendedBoolean.TRUE);

			doc.save("C:\\Users\\���������\\Documents\\JavaWord\\"+s+".docx");
            

		

	}
}