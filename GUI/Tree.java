import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.apache.pdfbox.exceptions.COSVisitorException;

public class Tree {
	private  JLabel l;
	private JFrame f;
	private  int size = 14;
	private  JTextField tf = new JTextField(10);
	private  String newString = new String();
	private  DefaultListModel<Object> lm;
	private  JList<Object> list;
	private JButton pdfButton;
	private JButton wordButton;
	private JButton photoButton;
	private JButton playMusic;
	private String pdfString;
	private JTextField pdfField;
	private JTextField wordField;
	private JPanel northPanel;
	private JPanel infoPanel;

	public void createGui() {
		f = new JFrame("���������");
		JPanel viewZone = new JPanel();
		l = new JLabel("��� ������");
		DefaultMutableTreeNode level0 = new DefaultMutableTreeNode(
				"���� ���������");
		JTree tree = new JTree(level0);
		viewZone.add(l);
		viewZone.setBackground(Color.WHITE);
		viewZone.setOpaque(true);
		viewZone.setVisible(false);
		JPanel contentPanel = new JPanel(new BorderLayout());
		JPanel buttonPanel = createFileButtonPanel();
		 lm = new DefaultListModel<Object>();
		 list = new JList<Object>(lm);
		buttonPanel.setBackground(Color.LIGHT_GRAY);
		buttonPanel.setOpaque(true);
		contentPanel.add(buttonPanel, BorderLayout.NORTH);
		JPanel rename = new JPanel();
		JLabel nameLab = new JLabel("������� ���");
		rename.add(nameLab);
		rename.add(tf);
		tf.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent k){
				if(k.getKeyCode() == KeyEvent.VK_ENTER){
					newString = tf.getText();
					rename.setVisible(false);
					String s = (String) list.getSelectedValue();
					s = s.trim();
					String[] s1 = s.split("[|]");
					s1[0] = s1[0].trim();
					s1[1] = s1[1].trim();
					File openfile = new File(s1[1] + s1[0]);
					String format[] = s1[0].split("[.]");
					format[1] = format[1].trim();
					File file = new File(s1[1]+newString+"."+format[1]);
					openfile.delete();
					int sel = list.getSelectedIndex();
					lm.remove(sel);
					if (!file.exists()) {
						try {
							file.createNewFile();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					lm.add(sel, "  "+newString+"."+format[1]+"          "+"|"+"  "+s1[1]);
					list.setSelectedIndex(sel);
					list.requestFocus();
					tf.setText(new String());
				}
			}
		});
		contentPanel.add(rename, BorderLayout.SOUTH);
		rename.setVisible(false);
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("����");
		menu.setMnemonic(KeyEvent.VK_ALT);
		JMenuItem menuItem = new JMenuItem("�������");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JFrame f1 = new JFrame("�������");
				JPanel fPanel = new JPanel();
				JLabel fLab = new JLabel("<html> <center><font size = 4>� ������� ��� ��������� �� ������<br> ������� ��������� ������� PDF � Word<br> � ������� ��������������� java ���������,<br> � ������� ��� ���������� �� ������ ������� ���� � ����������� ���,<br> � ������� ��� ������ ��������� ���������� � ������� ������<br> ���������� � ����������� java ����������");
				fLab.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
				fPanel.add(fLab);
				f1.getContentPane().add(fPanel);
				f1.setBounds(400, 200, 200, 200);
				f1.setMinimumSize(new Dimension(450, 300));
				f1.setLocationRelativeTo(null);
				f1.pack();
				f1.setVisible(true);
			}
		});
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		menu.add(menuItem);
		menuItem = new JMenuItem("� ���������");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JFrame f2 = new JFrame("� ���������");
				JPanel fPanel = new JPanel();
				JLabel fLab = new JLabel("<html> <center><font size = 4>��������� ��������� �������� ������ ����, ��������� ���, ���������������,<br>��������� ����� ����, ������� �����.<br> ���������� ������������� ���������<br>��� ������ � <strong>word, pdf, ����, �������</strong><br>������������������ � ��������������� �������� </font></center></html>");
			fLab.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
				fPanel.add(fLab);
				f2.getContentPane().add(fPanel);
				f2.setBounds(400, 200, 200, 200);
				f2.setMinimumSize(new Dimension(450, 300));
				f2.setLocationRelativeTo(null);
				f2.pack();
				f2.setVisible(true);
			}
		});
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		menu.add(menuItem);
		menu.addSeparator();
		menuItem = new JMenuItem("�����");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				Object[] options = { "��", "���!" };
				JPanel pan = new JPanel();
				int n = JOptionPane
						.showOptionDialog(pan, "������� ����?",
								"�������������", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE, null, options,
								options[0]);
				if (n == 0) {
					f.setVisible(false);
					System.exit(0);
				}
			}
		});
		menu.add(menuItem);
		menuBar.add(menu);
		menu = new JMenu("������");
		menuItem = new JMenuItem("�������");
		UIManager.put("FileChooser.openButtonText", "save");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JFileChooser chooser = new JFileChooser(".");
				int status = chooser.showOpenDialog(f);
				if (status == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					if (!file.exists()) {
						try {
							file.createNewFile();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem("�������");
		menuItem.addActionListener(new DelListener());
		menu.add(menuItem);
		menuItem = new JMenuItem("�������������");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(lm.size()!=0){
				   rename.setVisible(true);
				   tf.requestFocus();
				   tf.setCaretColor(Color.BLUE);
				}
				
			}
		});
		menu.add(menuItem);
		menuBar.add(menu);
		menu = new JMenu("���");
		menuItem = new JMenuItem("���������");
		menuItem.addActionListener(new BigListener());
		menu.add(menuItem);
		menuItem = new JMenuItem("���������");
		menuItem.addActionListener(new SmallListener());
		menu.add(menuItem);
		menuItem = new JMenuItem("������� ���������");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (viewZone.isVisible()) {
					viewZone.setVisible(false);
				} else {
					viewZone.setVisible(true);
				}
			}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem("�����������");
		menuItem.addActionListener(new SortListener());
		menu.add(menuItem);
		menuBar.add(menu);
		f.setJMenuBar(menuBar);

		contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 4, 4, 4));

		tree.setSelectionRow(0);
		ImageIcon leafIcon = new ImageIcon("images/treeNode.gif");
		DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
		renderer.setLeafIcon(leafIcon);
		tree.setCellRenderer(renderer);
		File[] roots = File.listRoots();
		DefaultMutableTreeNode level[] = new DefaultMutableTreeNode[100];
		DefaultMutableTreeNode docs = new DefaultMutableTreeNode("��� ���������");
		DefaultMutableTreeNode photos = new DefaultMutableTreeNode("��� ����������");
		DefaultMutableTreeNode musics = new DefaultMutableTreeNode("��� ������");
		for (int i = 0; i < roots.length - 1; i++) {
			level[0] = new DefaultMutableTreeNode(roots[i]);
			File[] children = roots[i].listFiles();
			for (int j = 0; j < children.length; j++) {
				if (children[j].isDirectory()) {
					level[1] = new DefaultMutableTreeNode(children[j]);
					level[0].add(level[1]);
					File children2[] = children[j].listFiles();
					if (children2 != null) {
						for (int g = 0; g < children2.length; g++) {
							if (children2[g].isDirectory()) {
								level[2] = new DefaultMutableTreeNode(
										children2[g]);
								level[1].add(level[2]);
								File children3[] = children2[g].listFiles();

								if (children3 != null && children3.length < 100) {
									for (int m = 0; m < children3.length; m++) {
										if (children3[m].isDirectory()) {
											level[3] = new DefaultMutableTreeNode(
													children3[m]);
											level[2].add(level[3]);
											File children4[] = children3[m]
													.listFiles();
											if (children4 != null
													&& children4.length < 50) {
												for (int b = 0; b < children4.length; b++) {
													if (children4[b]
															.isDirectory()) {
														level[4] = new DefaultMutableTreeNode(
																children4[b]);
														level[3].add(level[4]);
														if (children4[b]
																.getAbsolutePath()
																.equals("C:\\Users\\���������\\Documents\\JavaWord") || children4[b]
																		.getAbsolutePath()
																		.equals("C:\\Users\\���������\\Documents\\JavaPDF")) {
															DefaultMutableTreeNode doc = new DefaultMutableTreeNode(
																	children4[b]);
															
															docs.add(doc);
															
														}
														if(children4[b]
																.getAbsolutePath()
																.equals("C:\\Users\\���������\\Documents\\JavaPhoto")){
															DefaultMutableTreeNode photo = new DefaultMutableTreeNode(
																	children4[b]);
															
															photos.add(photo);
														}
														if(children4[b]
																.getAbsolutePath()
																.equals("C:\\Users\\���������\\Documents\\JavaMusic")){
															DefaultMutableTreeNode music = new DefaultMutableTreeNode(
																	children4[b]);
															
															musics.add(music);
														}
													}
												}
											}
										}
									}
								}
							}
						}

					}
				}
			}

			level0.add(level[0]);
		}
		level0.add(docs);
		level0.add(photos);
		level0.add(musics);
		tree.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				lm.removeAllElements();
				l.setText("��� ������");
				northPanel.setVisible(false);
				infoPanel.setVisible(false);
				try {
					DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) tree
							.getSelectionPath().getLastPathComponent();
					if (currentNode.getUserObject() instanceof File) {
						File[] children = ((File) currentNode.getUserObject())
								.listFiles();
						if(tree.getSelectionPath().getLastPathComponent().toString().equals("C:\\Users\\���������\\Documents\\JavaPDF")){
							pdfButton.setVisible(true);
							wordButton.setVisible(false);
							photoButton.setVisible(false);
							playMusic.setVisible(false);
							pdfField.requestFocus();
						}
						else if(tree.getSelectionPath().getLastPathComponent().toString().equals("C:\\Users\\���������\\Documents\\JavaWord")){
							wordButton.setVisible(true);
							pdfButton.setVisible(false);
							photoButton.setVisible(false);
							playMusic.setVisible(false);
							wordField.requestFocus();
						}
						else if(tree.getSelectionPath().getLastPathComponent().toString().equals("C:\\Users\\���������\\Documents\\JavaPhoto")){
							wordButton.setVisible(false);
							pdfButton.setVisible(false);
							photoButton.setVisible(true);
							playMusic.setVisible(false);
						}
						else if(tree.getSelectionPath().getLastPathComponent().toString().equals("C:\\Users\\���������\\Documents\\JavaMusic")){
							wordButton.setVisible(false);
							pdfButton.setVisible(false);
							photoButton.setVisible(false);
							playMusic.setVisible(true);
						}
						else{
							photoButton.setVisible(false);
							pdfButton.setVisible(false);
							wordButton.setVisible(false);
							playMusic.setVisible(false);
						}
						for (int j = 0; j < children.length; j++) {
							if (children[j].isFile()) {
								String name = children[j].getName();
								String path = children[j].getParent();
								
								int probel = 20 - children[j].getName()
										.length();
								for (int i = 0; i < probel; i++) {
									name += " ";
								}
								String s = "   " + name + "    |     " + path
										+ "\\" + "   ";
								lm.addElement(s);
								list.setSelectedIndex(0);
								list.requestFocus();
								
							}
						}
					} else {
						photoButton.setVisible(false);
						pdfButton.setVisible(false);
						wordButton.setVisible(false);
					}

				} catch (Exception e) {
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

		});

		list.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					if (list.getSelectedValue() instanceof String) {
						String s = (String) list.getSelectedValue();
						s = s.trim();
						String[] s1 = s.split("[|]");
						s1[0] = s1[0].trim();
						s1[1] = s1[1].trim();
						l.setText(s1[0]);
						File openfile = new File(s1[1] + s1[0]);
						try {
							Desktop.getDesktop().open(openfile);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, "���� �� ����� ���� ������! ���������� ���������� ��� ���");
						}
					}
				}
			}
		});
		list.addListSelectionListener(new ListSelectionListener(){

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				// TODO Auto-generated method stub
				if (list.getSelectedValue() instanceof String) {
					String s = (String) list.getSelectedValue();
					s = s.trim();
					String[] s1 = s.split("[|]");
					s1[0] = s1[0].trim();
					s1[1] = s1[1].trim();
					l.setText(s1[0]);
				}
			}
			
		});
		// ...........................................
		JScrollPane treeScrollPane = new JScrollPane(tree);
		JScrollPane textAreaScrollPane = new JScrollPane(list);
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				true, treeScrollPane, textAreaScrollPane);
		splitPane.setDividerLocation(348);
		contentPanel.add(splitPane, BorderLayout.CENTER);
		contentPanel.add(viewZone, BorderLayout.EAST);
		tree.requestFocus();
		list.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent k){
				if(k.getKeyCode() == KeyEvent.VK_DELETE){
					if (lm.size() != 0) {
						String s = (String) list.getSelectedValue();
						s = s.trim();
						String[] s1 = s.split("[|]");
						s1[0] = s1[0].trim();
						s1[1] = s1[1].trim();
						File openfile = new File(s1[1] + s1[0]);
						if (openfile.delete())
							System.out.println("delete!");
						lm.remove(list.getSelectedIndex());
						if (lm.size() != 0) {
							list.setSelectedIndex(0);
							list.requestFocus();
						}
					}
				}
			}
		});
		f.getContentPane().add(contentPanel);
		f.setBounds(400, 200, 200, 200);
		f.setMinimumSize(new Dimension(950, 700));
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);
	}

	public JPanel createFileButtonPanel() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		JPanel southPanel = new JPanel();
		northPanel = new JPanel();
		infoPanel = new JPanel();
		JButton del = new JButton(new ImageIcon("images/del.jpg"));
		del.addActionListener(new DelListener());
		JButton sort = new JButton(new ImageIcon("images/sort.jpg"));
		sort.addActionListener(new SortListener());
		JButton big = new JButton(new ImageIcon("images/big.jpg"));
		big.addActionListener(new BigListener());
		JButton small = new JButton(new ImageIcon("images/small.jpg"));
		small.addActionListener(new SmallListener());
		
		pdfField = new JTextField(10);
		pdfField.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent k){
				if(k.getKeyCode() == KeyEvent.VK_ENTER){
					pdfString = pdfField.getText();
					PDF pdf = new PDF();
					try {
						pdf.createPDF(pdfString);
					} catch (COSVisitorException e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "������� ������ ���!");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "������� ������ ���!");
					}
					pdfField.setText(new String());
					northPanel.setVisible(false);
					JOptionPane.showMessageDialog(null, "�������� ������!");
					String s = "   " + pdfString+".pdf" + "              |     " + "C:\\Users\\���������\\Documents\\JavaWord"
							+ "\\" + "   ";
					lm.addElement(s);
					list.setSelectedIndex(lm.size()-1);
					list.requestFocus();
				}
			}
		});
		wordField = new JTextField(10);
		wordField.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent k){
				if(k.getKeyCode() == KeyEvent.VK_ENTER){
					pdfString = wordField.getText();
					AddTable docx = new AddTable();
					try {
						docx.createWord(pdfString);
					} 
					catch(Exception e){
						JOptionPane.showMessageDialog(null, "������� ������ ���!");
					}
					wordField.setText(new String());
					northPanel.setVisible(false);
					JOptionPane.showMessageDialog(null, "�������� ������!");
					String s = "   " + pdfString+".docx" + "                   |     " + "C:\\Users\\���������\\Documents\\JavaWord"
							+ "\\" + "   ";
					lm.addElement(s);
					list.setSelectedIndex(lm.size()-1);
					list.requestFocus();
				}
			}
		});
		
		pdfButton = new JButton("PDF");
		pdfButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				northPanel.remove(wordField);
				northPanel.add(pdfField);
				northPanel.setVisible(true);
				infoPanel.setVisible(true);
				pdfField.requestFocus();
			}
		});
		pdfButton.setVisible(false);
		wordButton = new JButton("Word");
		wordButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				northPanel.remove(pdfField);
				northPanel.add(wordField);
				northPanel.setVisible(true);
				infoPanel.setVisible(true);
				wordField.requestFocus();
			}
		});
		wordButton.setVisible(false);
		photoButton = new JButton("������� �����");
		photoButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (list.getSelectedValue() instanceof String) {
					String s = (String) list.getSelectedValue();
					s = s.trim();
					String[] s1 = s.split("[|]");
					s1[0] = s1[0].trim();
					s1[1] = s1[1].trim();
					l.setText(s1[0]);
					File openfile = new File(s1[1] + s1[0]);
					try{
					Main.createGray(openfile);
					}
					catch(Exception exc){
						
					}
					String str = "   gray" + s1[0] + "                   |     " + "C:\\Users\\���������\\Documents\\JavaWord"
							+ "\\" + "   ";
					lm.addElement(str);
					list.setSelectedIndex(lm.size()-1);
					list.requestFocus();
				}
				
			}
		});
		photoButton.setVisible(false);
		playMusic = new JButton(new ImageIcon("images/music.jpg"));
		playMusic.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				PlayVideo pv = new PlayVideo();
				try {
					pv.load(new File(
								"C:\\Users\\���������\\Documents\\JavaMusic\\dict.wav"));
				} catch (Exception exc) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "�� ������� ��������� �����������");
				}
			}
		});
		playMusic.setVisible(false);
		southPanel.add(del);
		southPanel.add(sort);
		southPanel.add(big);
		southPanel.add(small);
		southPanel.add(pdfButton);
		southPanel.add(wordButton);
		southPanel.add(photoButton);
		southPanel.add(playMusic);
		JLabel l = new JLabel("��� ���������: ");
		JLabel inf = new JLabel("�������� ����� �������� � ������� java ����������");
		northPanel.add(l);
		infoPanel.add(inf);
		buttonPanel.add(southPanel, BorderLayout.NORTH);
		buttonPanel.add(infoPanel, BorderLayout.CENTER);
		buttonPanel.add(northPanel, BorderLayout.SOUTH);
		buttonPanel.setBackground(Color.LIGHT_GRAY);
		northPanel.setVisible(false);
		infoPanel.setVisible(false);
		buttonPanel.setOpaque(true);
		return buttonPanel;
	}

	public class DelListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (lm.size() != 0) {
				String s = (String) list.getSelectedValue();
				s = s.trim();
				String[] s1 = s.split("[|]");
				s1[0] = s1[0].trim();
				s1[1] = s1[1].trim();
				File openfile = new File(s1[1] + s1[0]);
				if (openfile.delete())
				{
					
				}
				else{
					JOptionPane.showMessageDialog(null, "���� �� ����� ���� ������!");
				}
				lm.remove(list.getSelectedIndex());
				if (lm.size() != 0) {
					list.setSelectedIndex(0);
					list.requestFocus();
				}
			}
		}
		
	}
	public class SortListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (lm.size() != 0) {
				String names[] = new String[lm.size()];
				String paths[] = new String[lm.size()];
				for (int i = 0; i < lm.size(); i++) {
					String s = (String) lm.getElementAt(i);
					s = s.trim();
					String[] s1 = s.split("[|]");
					names[i] = s1[0];
					paths[i] = s1[1];
				}
				Arrays.sort(names, Collections.reverseOrder());
				lm.removeAllElements();
				for(int i = 0; i < names.length; i++){
					
					lm.addElement("   "+names[i]+"|"+paths[i]);
				}
				if (lm.size() != 0) {
					list.setSelectedIndex(0);
					list.requestFocus();
				}
			}
		}
		
	}
	
	public class BigListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			list.setFont(new Font("Veranda", Font.PLAIN, ++size));
		}
		
	}
	public class SmallListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			list.setFont(new Font("Veranda", Font.PLAIN, --size));
		}
		
	}
	
}
