import javax.swing.*;
import javax.media.*;

import java.awt.*;
import java.net.*;
import java.io.*;

@SuppressWarnings("serial")
public class PlayVideo extends JFrame {

	private Player player;
	private Component center;
	private Component south;

	public void load(final File file) throws Exception {
		JPanel p = new JPanel();
		p.add(new JLabel(new ImageIcon("images/player.jpg"), SwingConstants.CENTER));

		getContentPane().add(p);
		pack();
		setVisible(true);
		setLocationRelativeTo(null);
		setMinimumSize(new Dimension(300, 200));
		URL url = file.toURL();
		final Container contentPane = getContentPane();
		if (player != null) {
			player.stop();
		}
		player = Manager.createPlayer(url);
		ControllerListener listener = new ControllerAdapter() {
			public void realizeComplete(RealizeCompleteEvent event) {
				Component vc = player.getVisualComponent();
				if (vc != null) {
					contentPane.add(vc, BorderLayout.CENTER);
					center = vc;
				} else {
					if (center != null) {
						contentPane.remove(center);
						contentPane.validate();
					}
				}
				Component cpc = player.getControlPanelComponent();
				if (cpc != null) {
					contentPane.add(cpc, BorderLayout.SOUTH);
					south = cpc;
				} else {
					if (south != null) {
						contentPane.remove(south);
						contentPane.validate();
					}
				}
				pack();
				setTitle(file.getName());
			}
		};
		player.addControllerListener(listener);
		player.start();
	}

}