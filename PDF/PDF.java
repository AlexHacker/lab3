import java.io.IOException;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;


public class PDF {

	public  void createPDF(String s) throws COSVisitorException, IOException {
		PDDocument doc = new PDDocument();

         PDPage page = new PDPage();
         doc.addPage( page );
         PDFont font = PDType1Font.HELVETICA_BOLD;

         PDPageContentStream contentStream = new PDPageContentStream(doc, page);
         contentStream.beginText();
         contentStream.setFont( font, 12 );
         contentStream.moveTextPositionByAmount( 100, 700 );
         contentStream.drawString( "Hellow World!" );
         contentStream.endText();
         contentStream.close();
         doc.save("C:\\Users\\Александр\\Documents\\JavaPDF\\"+s+".pdf");

	}

}
